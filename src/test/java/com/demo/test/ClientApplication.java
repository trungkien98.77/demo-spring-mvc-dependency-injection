package com.demo.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.demo.configuration.DIConfiguration;
import com.demo.consumer.MyApplication;

public class ClientApplication {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
//		context.scan("com.demo.consumer");
		MyApplication app = context.getBean(MyApplication.class);
		
		app.processMessage("Hi Pankaj", "pankaj@abc.com");
		
		//close the context
		context.close();
	}

}