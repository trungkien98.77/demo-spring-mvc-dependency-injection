package com.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.demo.service.EmailService;
import com.demo.service.MessageService;

@Configuration
@ComponentScan(value = {"com.demo.consumer"})
public class DIConfiguration {

  @Bean
  public MessageService getMessageService() {
    return new EmailService();
  }
}
