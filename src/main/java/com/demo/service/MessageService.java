package com.demo.service;

public interface MessageService {

  boolean sendMessage(String msg, String rec);
}
